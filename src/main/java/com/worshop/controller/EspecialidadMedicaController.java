package com.worshop.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.worshop.models.EspecialidadMedica;
import com.worshop.service.EspecialidadMedicaService;

@RestController
//sirve para decir que la clase sera de servicios rest que son interoperables 
@RequestMapping("/especialidad")
//sirve para identificar donde estaran los servicios para hacer una consulta post, get o weas
public class EspecialidadMedicaController {

	@Autowired
	EspecialidadMedicaService service;
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EspecialidadMedica>> listar(){
		List<EspecialidadMedica> especialistas=new ArrayList<>();
		especialistas=service.getAll();
		return new ResponseEntity<List<EspecialidadMedica>> (especialistas, HttpStatus.OK);
				
	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public EspecialidadMedica registrar(@RequestBody EspecialidadMedica v) {
		return service.persist(v);
	}
	
	@GetMapping(value ="/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EspecialidadMedica> listarId(@PathVariable("id") Integer id){
		EspecialidadMedica especialista = service.findById(id);
		return new ResponseEntity<EspecialidadMedica> (especialista, HttpStatus.OK);
				
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity <Object> actualizar(@Valid @RequestBody EspecialidadMedica especialidad) {
		service.merge(especialidad);
		return new ResponseEntity<Object> (HttpStatus.OK);
	}
	@DeleteMapping(value ="/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
		EspecialidadMedica espec = service.findById(id);
		service.delete(id);
		
	}

}
