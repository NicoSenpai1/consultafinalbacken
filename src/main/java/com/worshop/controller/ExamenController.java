package com.worshop.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.worshop.models.Examen;
import com.worshop.service.ExamenService;

@RestController
//sirve para decir que la clase sera de servicios rest que son interoperables 
@RequestMapping("/examen")
//sirve para identificar donde estaran los servicios para hacer una consulta post, get o weas
public class ExamenController {

	@Autowired
	ExamenService service;
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Examen>> listar(){
		List<Examen> examenes=new ArrayList<>();
		examenes=service.getAll();
		return new ResponseEntity<List<Examen>> (examenes, HttpStatus.OK);
				
	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Examen registrar(@RequestBody Examen v) {
		return service.persist(v);
	}
	
	@GetMapping(value ="/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Examen> listarId(@PathVariable("id") Integer id){
		Examen especialista = service.findById(id);
		return new ResponseEntity<Examen> (especialista, HttpStatus.OK);
				
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity <Object> actualizar(@Valid @RequestBody Examen examens) {
		service.merge(examens);
		return new ResponseEntity<Object> (HttpStatus.OK);
	}
	@DeleteMapping(value ="/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
		Examen exam = service.findById(id);
		service.delete(id);
		
	}

}
