package com.worshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.worshop.models.Usuario;

public interface UsuarioDAO extends JpaRepository<Usuario, Integer> {
		
	Usuario findOneByUsername(String username);
}