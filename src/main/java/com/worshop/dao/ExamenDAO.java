package com.worshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.worshop.models.Examen;
@Repository
public interface ExamenDAO extends JpaRepository<Examen, Integer>{

	
}
