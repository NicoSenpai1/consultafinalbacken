package com.worshop.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worshop.dao.ExamenDAO;
import com.worshop.models.Examen;

@Service
public class ExamenService implements com.worshop.service.ExamenService{

	@Autowired
	ExamenDAO service;
	@Override
	public Examen persist(Examen e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public List<Examen> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public Examen findById(Integer id) {
		// TODO Auto-generated method stub
		return service.findOne(id);
	}

	@Override
	public Examen merge(Examen e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
	}
	
}
