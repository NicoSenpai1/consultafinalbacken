package com.worshop.service;

import java.util.List;

import com.worshop.models.Examen;

public interface ExamenService {
	Examen persist(Examen e);
	//mostrar lista de especialidad medica
		List<Examen> getAll();
	//traer una especialdad mediante un id
		Examen findById(Integer id);
	//actualizar
		Examen merge(Examen e);
	//eliminar
		void delete(Integer id);	
}
