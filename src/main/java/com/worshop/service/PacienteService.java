package com.worshop.service;

import java.util.List;

import com.worshop.models.Paciente;

public interface PacienteService {
	Paciente persist(Paciente e);
	//mostrar lista de especialidad medica
		List<Paciente> getAll();
	//traer una especialdad mediante un id
		Paciente findById(Integer id);
	//actualizar
		Paciente merge(Paciente e);
	//eliminar
		void delete(Integer id);	
}
