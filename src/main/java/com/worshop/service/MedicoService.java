package com.worshop.service;

import java.util.List;

import com.worshop.models.Medico;

public interface MedicoService {

	Medico persist(Medico e);
	//mostrar lista de especialidad medica
		List<Medico> getAll();
	//traer una especialdad mediante un id
		Medico findById(Integer id);
	//actualizar
		Medico merge(Medico e);
	//eliminar
		void delete(Integer id);
}
