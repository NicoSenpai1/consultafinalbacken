package com.worshop.service;

import java.util.List;

import com.worshop.models.EspecialidadMedica;
//esta clase sirve para determinar las funciones que podremos hacer en la tabla
//osea los servicios que podremos hacerles a la tabla

public interface EspecialidadMedicaService {
//guardad una especialidad medica
	EspecialidadMedica persist(EspecialidadMedica e);
//mostrar lista de especialidad medica
	List<EspecialidadMedica> getAll();
//traer una especialdad mediante un id
	EspecialidadMedica findById(Integer id);
//actualizar
	EspecialidadMedica merge(EspecialidadMedica e);
//eliminar
	void delete(Integer id);	
	
	
	
	
	
	
	
	
	
	
}
