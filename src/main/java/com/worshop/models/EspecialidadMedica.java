package com.worshop.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
//sirve para crear una entidad, en este caso, una tabla
public class EspecialidadMedica {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//sirve para aumentar automaticamente el id
	private Integer id;
	private String descripcion;
	@Column (name="descripcion", nullable = false, unique= true) 
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
