package com.worshop.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="paciente")
public class Paciente {
//arreglar problema en no poder insertar valores con el postman
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column (name="nombre",nullable = false)	
	private String nombre;
	@Column (name="rut",nullable = false)	
	private String rut;
	@Column (name="email",nullable = false)
	private String email;
	@Column (name="telefono",nullable = false)
	private String telefono;
	@Column (name="sangre",nullable = false)
	private String sangre;
	@Column (name="fecha",nullable = false)
	private Integer fecha;
	public Integer getIdPaciente() {
		return id;
	}
	public void setIdPaciente(Integer idPaciente) {
		this.id = idPaciente;
	}
	public String getNombrePaciente() {
		return nombre;
	}
	public void setNombrePaciente(String nombrePaciente) {
		this.nombre = nombrePaciente;
	}
	public String getRutPaciente() {
		return rut;
	}
	public void setRutPaciente(String rutPaciente) {
		this.rut = rutPaciente;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNumTelefono() {
		return telefono;
	}
	public void setNumTelefono(String numTelefono) {
		this.telefono = numTelefono;
	}
	public String getTipoSangre() {
		return sangre;
	}
	public void setTipoSangre(String tipoSangre) {
		this.sangre = tipoSangre;
	}
	public Integer getFecha() {
		return fecha;
	}
	public void setFecha(Integer fecha) {
		this.fecha = fecha;
	}
	
}
